﻿##Description
This was a group project where we created a complete digital marketing plan for a monthly subscription, healthy meal delivery, start-up. Deliverables included:

- Product or Service document
	* Here we gave an indepth description of what we provide to our customers
- Customer Journey

	* Awareness
	* Consideration
	* Purchase
	* Loyalty
	* Advocay

- Final Report
	* Website Mockups
 	* Business Canvas Model
 	* SEO optimization
 
- PowerBI Dashboard with KPIs
